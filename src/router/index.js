import { createRouter, createWebHistory } from 'vue-router'
import NoteList from '../components/NoteList.vue'
import Home from '../components/Home.vue'
import AddNote from '../components/AddNote.vue'
import Note from '../components/Note.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/all-notes',
      name: 'allnotes',
      component: NoteList // todo: make this in a home page that has a menu to navigate between sections
    },
    {
      path: '/add-note',
      name: 'addnote',
      component: AddNote // todo: make this in a home page that has a menu to navigate between sections
    },
    {
      path: '/note/:id',
      name: 'note',
      component: Note // todo: make this in a home page that has a menu to navigate between sections
    }

    // todo: editor for saving new note
    // todo: editor to show or edit a note
  ]
})

export default router

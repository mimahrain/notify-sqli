import { defineStore } from 'pinia'
import axios from 'axios'

export const useNotesStore = defineStore('notes', {
  state: () => ({
    notes: []
  }),
  getters: {
    // getNoteByID
    getNoteById: (state) => (id) => {
      return state.notes.find((note) => note.id === id)
    }
  },
  actions: {
    // fetch all notes
    async fetchNotes() {
      try {
        const response = await axios.get('http://localhost:3000/notes')
        this.notes = response.data
      } catch (error) {
        console.error('ERROR Fetching Notes :', error)
      }
    },
    // post note
    async postNote(note) {
      try {
        const response = await axios.post('http://localhost:3000/notes', note)
      } catch (error) {
        console.log('Error: ', error)
      }
    },
    // delete note
    async deleteNote(id) {
      try {
        const response = await axios.delete('http://localhost:3000/notes/'+id)
      } catch (error) {
        console.log('Error: ', error)
      }
    },
    // update note
    async updateNote(id,note) {
      try {
        const response = await axios.put('http://localhost:3000/notes/'+id, note)
      } catch (error) {
        console.log('Error: ', error)
      }
    }
  }
})
